extern crate tungstenite;
extern crate url;
extern crate csv;
extern crate timely;
extern crate differential_dataflow;
extern crate serde_json;
extern crate event_horizon;

use std::time::SystemTime;
use differential_dataflow::input::InputSession;
use serde_json::Value;
use event_horizon::websockets::WebSockets;

fn main() {
    timely::execute_from_args(std::env::args(), move |worker| {

        let mut input = InputSession::new();
        let file_path = std::path::Path::new("result_text.csv");
        let mut wtr = csv::Writer::from_path(file_path).unwrap();

        // init exchange socket
        let mut okex_socket = WebSockets::new();
        okex_socket.connect("wss://real.okex.com:10441/websocket").unwrap();

        okex_socket.write_message("{'event':'addChannel','channel':'ok_sub_spot_eth_usdt_depth_10'}").unwrap();

        let mut binance_socket = WebSockets::new();
        binance_socket.connect("wss://stream.binance.com:9443/ws/ethusdt@depth10").unwrap();

        worker.dataflow(|scope| {

        let manages = input.to_collection(scope);
            manages
                .map(|m1: String| m1)
                .inspect(move |x| {
                    let str_x = &x.0[..];
                    let vec: Vec<&str> = str_x.split(',').collect();
                    wtr.write_record(&vec).unwrap();
                    println!("{:?}", x)
                });
        });

        input.advance_to(0);

        loop {
            let start = SystemTime::now();
            let since_the_epoch = start.duration_since(SystemTime::UNIX_EPOCH);
            let current_time = since_the_epoch.unwrap().as_secs().to_string();

            // loop till socket connection working
            loop {
                if let Err(e) = okex_socket.fetch_message() {
                    // re-connect socket if it's unexpected failed
                    okex_socket = WebSockets::new();
                    okex_socket.connect("wss://real.okex.com:10441/websocket").unwrap();
                    okex_socket.write_message("{'event':'addChannel','channel':'ok_sub_spot_eth_usdt_depth_10'}").unwrap();
                } else {
                    // nothing error
                    break;
                }
            }
            
            // loop till socket connection working
            loop {
                if let Err(e) = binance_socket.fetch_message() {
                    // re-connect socket if it's unexpected failed
                    binance_socket = WebSockets::new();
                    binance_socket.connect("wss://stream.binance.com:9443/ws/ethusdt@depth10").unwrap();
                } else {
                    // nothing error
                    break;
                }
            }
            
            let msg_okex = okex_socket.get_message();
            let msg_binance = binance_socket.get_message();

            let time = current_time.parse::<i32>().unwrap();

            let v_m1: Value = match serde_json::from_str(&msg_okex) {
                Ok(val) => val,
                Err(_) => { 
                    serde_json::from_str("{}").expect("error parse json")
                }
            };
             
            let v_m2: Value = match  serde_json::from_str(&msg_binance) {
                Ok(val) => val,
                Err(_) => { 
                    serde_json::from_str("{}").expect("error parse json")
                }
            };

            let name_exhange1 = String::from("OKex");
            let name_exchange2 = String::from("Binance");

            let time_exchange1 = format!("{}", v_m1[0]["data"]["timestamp"]);
            let time_exchange2 = String::from("");

            let ask_price_exchange1 = format!("{}", v_m1[0]["data"]["asks"][0][0]);

            let ask_price_exchange1_volume = format!("{}", v_m1[0]["data"]["asks"][0][1]);

            let bid_price_exchange2 = format!("{}", v_m2["bids"][0][0]);
            let bid_price_exchange2_volume = format!("{}", v_m2["bids"][0][1]);

            let ask_price_exchange2 = format!("{}", v_m2["asks"][0][0]);
            let ask_price_exchange2_volume = format!("{}", v_m2["asks"][0][1]);

            let bid_price_exchange1 = format!("{}", v_m1[0]["data"]["bids"][0][0]);
            let bid_price_exchange1_volume = format!("{}", v_m1[0]["data"]["bids"][0][1]);

            let v_forward = format!("{},{},{},{},{},{},{},{},{},{},{},{}",
                            name_exhange1,
                            name_exchange2,
                            time_exchange1,
                            time_exchange2,
                            ask_price_exchange1,
                            ask_price_exchange1_volume,
                            bid_price_exchange2,
                            bid_price_exchange2_volume,
                            ask_price_exchange2,
                            ask_price_exchange2_volume,
                            bid_price_exchange1,
                            bid_price_exchange1_volume);

            // skip add channel data
            if msg_okex.find("addChannel") == None {
                input.insert(v_forward);
                input.advance_to(time);
                input.flush();
                worker.step();
            } else {
                input.advance_to(time);
                input.flush();
                worker.step();
            }

        }

    }).expect("Computation terminated abnormally");
}