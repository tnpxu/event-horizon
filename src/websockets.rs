use url::Url;
use errors::*;
use serde_json::from_str;

use tungstenite::connect;
use tungstenite::protocol::WebSocket;
use tungstenite::client::AutoStream;
use tungstenite::handshake::client::Response;
use tungstenite::Message;

// static OKEX_URL: &'static str = "wss://real.okex.com:10441/websocket";
// static BINANCE_URL: &'static str = "wss://stream.binance.com:9443/ws/ethusdt@depth10";

pub struct WebSockets {
    socket: Option<(WebSocket<AutoStream>, Response)>,
    socket_message: String
}

impl WebSockets {

    pub fn new() -> WebSockets {
            WebSockets {
                socket: None,
                socket_message: String::from("")
            }
    }

    pub fn connect(&mut self, EXCHANGE_URL: &str) -> Result<()> {
        let url = Url::parse(EXCHANGE_URL).unwrap();

        match connect(url) {
            Ok(answer) => {
                self.socket = Some(answer);
                Ok(())
            }
            Err(e) => {
                bail!(format!("Error during handshake {}", e))            
            }
        }
    }

    pub fn get_message(&mut self) -> String {
        self.socket_message.clone()
    }

    pub fn write_message(&mut self, send_msg: &str) -> Result<()> {
        if let Some(ref mut socket) = self.socket {

            match socket.0.write_message(Message::Text(send_msg.into())) {
                Ok(msg) => {
                    Ok(())
                }
                Err(e) => {
                    bail!("write message error") 
                }
            }

        } else {
            bail!("socket error at write_messsage method") 
        }
    }

    pub fn fetch_message(&mut self) -> Result<()> {
        if let Some(ref mut socket) = self.socket {
            let get_socket_message = socket.0.read_message()?;

            match get_socket_message.into_text() {
                Ok(msg) => {
                    self.socket_message = msg;
                    Ok(())
                }
                Err(e) => {
                    bail!("parse into_text error") 
                }
            }
        } else {
            bail!("socket error at fetch_message method") 
        }
    }
}